import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {StationComponent} from './station/station.component';
import {DetailsComponent} from './details/details.component';
import {PlayComponent} from './play/play.component';
import {HttpClientModule} from '@angular/common/http';
import {InfiniteScrollModule} from 'ngx-infinite-scroll';
import {ModalModule} from 'ngx-bootstrap';

@NgModule({
    declarations: [
        AppComponent,
        StationComponent,
        DetailsComponent,
        PlayComponent,
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        HttpClientModule,
        InfiniteScrollModule,
        ModalModule.forRoot()
    ],
    bootstrap: [AppComponent],
    exports: [
        InfiniteScrollModule
    ]
})
export class AppModule {
}
