export interface Station {
    id: number;
    name: string;
    wave: string;
    http: string;
    img: string;
}
