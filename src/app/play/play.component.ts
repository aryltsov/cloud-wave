import {
    Component,
    OnInit,
    AfterContentInit,
    ViewChild,
    ElementRef
} from '@angular/core';

import {filter, switchMap} from 'rxjs/operators';
import {ActivatedRoute, ParamMap} from '@angular/router';
import {RadioStationService} from '../stations.service';
import {Station} from '../station';

@Component({
    selector: 'app-play',
    templateUrl: './play.component.html',
    styleUrls: ['./play.component.css']
})
export class PlayComponent implements OnInit, AfterContentInit {
    @ViewChild('audio') audio: ElementRef;

    station: Station;
    play: boolean = true;
    loaded: boolean = false;

    constructor(
        private route: ActivatedRoute,
        private svc: RadioStationService
    ) {
    }

    ngOnInit() {
    }

    ngAfterContentInit() {
        this.route.paramMap.pipe(
            filter((params: ParamMap) => params.has('id')),
            switchMap((params) => this.svc.getStation(params.get('id')))
        ).subscribe((station) => {
            this.station = station;

            this.audio.nativeElement.onplaying = () => {
                this.loaded = true;
            };

            const playPromise = this.audio.nativeElement.play();
            if (playPromise) {
                playPromise.catch(() => {
                    this.audio.nativeElement.play();
                });
            }
        });
    }

    playPause(): void {
        this.play = !this.play;

        if (this.play) {
            this.audio.nativeElement.play();
        } else {
            this.audio.nativeElement.pause();
        }
    }
}
