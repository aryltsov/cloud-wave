import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {DetailsComponent} from './details/details.component';
import {PlayComponent} from './play/play.component';

const routes: Routes = [
    {path: '', redirectTo: 'station', pathMatch: 'full'},
    {
        path: 'station',
        children: [
            {
                path: ':id/details',
                component: DetailsComponent
            },
            {
                path: ':id/play',
                component: PlayComponent
            },
        ]
    },
    {path: '**', redirectTo: '/station', pathMatch: 'full'}
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
