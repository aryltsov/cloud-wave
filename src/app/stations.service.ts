import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {map} from 'rxjs/operators';
import {Station} from './station';
import {Observable} from 'rxjs';


@Injectable({
    providedIn: 'root'
})
export class RadioStationService {

    constructor(private http: HttpClient) {
    }

    getStations(from, quantity): Observable<Station[]> {
        return this.http.get<Station[]>('./assets/radio_ua.json').pipe(
            map((x: Station[]): Station[] => {
                return x.splice(from, quantity);
            })
        );
    }

    getStation(id): Observable<Station> {
        return this.http.get<Station[]>('./assets/radio_ua.json').pipe(
            map((x: Station[]): Station => {
                return x.find((station: Station) => +station.id === +id);
            })
        );
    }


}
