import {Component, OnInit, ViewChild, ElementRef} from '@angular/core';
import {ActivatedRoute, ParamMap, Router} from '@angular/router';
import {BsModalService, BsModalRef} from 'ngx-bootstrap/modal';
import {filter, switchMap} from 'rxjs/operators';
import {RadioStationService} from '../stations.service';
import {Station} from '../station';

@Component({
    selector: 'app-details',
    templateUrl: './details.component.html',
    styleUrls: ['./details.component.css']
})
export class DetailsComponent implements OnInit {
    @ViewChild('stationTemplate') stationTemplate: ElementRef;

    modalRef: BsModalRef;
    station: Station;

    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private svc: RadioStationService,
        private modalService: BsModalService
    ) {
    }

    ngOnInit() {
        this.route.paramMap.pipe(
            filter((params: ParamMap) => params.has('id')),
            switchMap((params) => this.svc.getStation(params.get('id')))
        ).subscribe((station: Station) => {
            this.station = station;
            this.modalRef = this.modalService.show(this.stationTemplate, {
                backdrop: 'static',
                keyboard: false
            });
        });

        this.router.events.subscribe((val: any) => {
            if (this.modalRef && val.url && (val.url === '/station' || val.url.search('play'))) {
                this.modalRef.hide();
            }
        });
    }

    closeModalAndRedirect(routeOptions): void {
        if (!this.modalRef) {
            return;
        }

        this.modalRef.hide();
        this.modalRef = null;

        this.router.navigate(routeOptions);
    }
}
