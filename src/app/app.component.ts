import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {RadioStationService} from './stations.service';
import {Station} from './station';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css'],
    providers: [RadioStationService]
})
export class AppComponent implements OnInit {
    private from: number = 0;
    private quantity: number = 10;
    radioStations: Station[] = [];

    constructor(
        private route: ActivatedRoute,
        private svc: RadioStationService
    ) {
    }

    ngOnInit() {
        this.getStations(this.from, this.quantity);
    }

    getStations(from, to): void {
        this.svc.getStations(from, to).subscribe(
            radioStations => {
                radioStations.forEach((station: Station) => {
                    this.radioStations.push(station);
                });
            }
        );
    }


    onScroll(): void {
        this.from += 1;
        this.quantity = 1;
        this.getStations(this.from, this.quantity);
    }
}
